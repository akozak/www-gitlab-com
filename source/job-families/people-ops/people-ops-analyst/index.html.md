---
layout: job_page
title: "People Operations Analyst"
---

## Responsibilities

- Develop compensation strategies and principles.
- HRIS management, ensuring Data Integrity and alignment with all ancillary systems.
- Process HRIS changes related to events, such as hiring, termination, leaves, transfers, or promotions.
- Develop and implement People Operations policies, processes and procedures following the GitLab workflow, with the goal always being to make things easier from the perspective of the team members.
- Maintain and update people operations documentation.
- [Manage the GitLab Handbook](/handbook/handbook-usage/#handbook-management), ensuring accuracy, and alignment with the [Handbook Usage](/handbook/handbook-usage/).
- Benefits management and development of benefits principles.
- Address employee relations issues, such as harassment allegations, work complaints, or other employee concerns.
- Collect and analyze data to track trends in functional areas.
- Ensure compliance with all international rules and regulations.
- Manage special projects.
- Train employees on various topics.
- Process and document all visa related applications and questions for travel and employment.
- Keep it efficient and DRY.

## Requirements

- Prior extensive experience in an HR or People Operations role
- Clear understanding of HR laws in one or multiple countries where GitLab is active
- Ability to work strange hours when needed (for example, to call an embassy in a different continent)
- Excellent written and verbal communication skills
- Enthusiasm for, and broad experience with, software tools
- Proven experience quickly learning new software tools
- Willing to work with git and GitLab whenever possible
- Willing to make People Operations as open and transparent as possible
- Wanting to work for a fast moving startup
- You share our [values](/handbook/values), and work in accordance with those values
- The ability to work in a fast paced environment with strong attention to detail is essential.
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).

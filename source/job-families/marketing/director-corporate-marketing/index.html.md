---
layout: job_page
title: "Director, Corporate Marketing"
---

As the Director, Corporate Marketing, you will lead the [Corporate Marketing team](/handbook/marketing/#corporate-marketing) responsible for building the GitLab brand and sharing the GitLab story globally. You understand that markets are fundamentally collections of people, and the more empathy you have for the people within the markets you serve, the more effective you will be. This role will promote the company and its customers through creative story-telling using corporate events, social media, video, design, PR, and written content marketing. The qualified candidate will be a driven and engaged senior leader with a successful track record of leading cross-functionally and inspiring a world class team. Reporting to the CMO, the Director, Corporate Marketing is a key member of the marketing leadership team at GitLab.

## Responsibilities

* Define the Corporate Marketing vision and strategy that integrates brand, design, content, corporate events, and PR to deliver powerful, market-moving messages to all of GitLab’s audiences, at scale.
* Manage and lead a global marketing team to evolve GitLab’s perception in the market from a version control tool to a single integrated application for the complete DevOps lifecycle.
* Work closely with other members of the marketing leadership team to define the strategic objectives for all of marketing and build strong working relationships to deliver as a team.
* Lead a world-class content marketing program that drives awareness of GitLab, the related business challenges, and how GitLab can help.
* Lead a world class team of designers to reinforce our brand and message, visually.
* Lead our corporate events strategy to amplify our brand at national and international trade shows, establish an EBC, and connect with media and analysts contacts.
* Serve as a company spokesperson as needed for certain media, analyst and public speaking engagements.
* Oversee GitLab Summit, and transform it from an employee gathering to a summit that brings together the extended GitLab community of customers, partners, contributors, and users.
* Arm our field marketing team with marketing communication materials to ensure that when we deploy marketing tactics regionally, they reinforce our strategy globally.
* Define GitLab’s brand personality, visual identity, and written style.
* Own the GitLab case study program to showcase and celebrate the incredible accomplishments of our customers.
* Own our outbound social media engagement strategy.
* Manage all corporate communications including executive communications, company messaging development, and PR.
* Create, architect and promote corporate content for all stages of the buyer journey.
* Support internal teams with creative marketing support such as writing, creative design, event planning, and video.
* Manage, build, and lead a strong team by coaching and developing existing members and closing talent gaps where needed through hiring new team members.

## Requirements

* 10+ years of experience in corporate marketing in the software industry, preferably within an area of application development.
* Technical background or good understanding of developer products; familiarity with Git, Continuous Integration, Containers, and Kubernetes a plus.
* Experience with Software-as-a-Service offerings and open core software a plus.
* Proven track record developing and executing successful B2B corporate communications, content and brand campaigns.
* Skilled and proven experience bringing creative ideas to life across a variety of communication tactics.
* Experience leading the development of exciting and effective awareness and brand marketing campaigns that influence IT leadership and CxO buyers.
* Track record of evolving a brand’s position to a new market category that aligns it with current market and customer priorities.
* History successfully utilizing events as a key platform for growth in awareness and brand preference.
* Proven track record in building, getting buy-in and executing marketing plans, and staying focused on “getting it done” in a fast-moving, technical environment.
* Able to coordinate across many teams and perform in a fast-moving startup environment.
* Proven ability to be self-directed and work with minimal supervision.
* Outstanding written and verbal communications skills with the ability to explain and translate complex technology concepts into simple and intuitive communications.
* Uses data to measure results and inform decision making and strategy development.
* You share our [values](/handbook/values), and work in accordance with those values.

## Hiring process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team/).

* Qualified candidates will be invited to schedule a 30 minute [screening call](handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* A 45 minute interview with our Director of Product Marketing
* A 45 minute interview with our Senior Director of Marketing and Sales Development
* A 45 minute interview with our Vice President of Product
* A 45 minute interview with our Chief Revenue Officer
* A 45 minute interview with our CEO (interim CMO)
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

---
layout: markdown_page
title: "2018 Q3 OKRs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### CEO: Grow Incremental ACV according to plan. IACV at 120% of plan, pipeline for Q4 at 3x IACV minus in quarter orders, LTV/CAC per campaign and type of customer.

* VPE
  * Support
    * Self-hosted: 95% on CSAT, 95% on Premium SLAs
    * Services: 95% on CSAT, 95% on Gold SLAs
* CMO
    * Growth Marketing: Achieve 3x IACV in Q4 pipeline minus in quarter orders
        * Content: Execute Cloud campaign to support demand generation. Produce 3 cloud native focused webinars with a total of 1000 registrations, publish 1 cloud native-focused case study.
        * Content: Execute Security campaign to support demand generation. Produce 3 security focused webinars with a total of 1000 registrations, publish 1 security-focused case study.
        * Content: Increase organic search traffic to accelerate awareness of GitLab brand and product capabilities. Publish 10 feature tutorial posts, publish /devops-culture, publish /devops-tools.
        * Field Marketing: Improve account based marketing for large & strategic accounts. Pull in 1 large or strategic deal into Q3, increase opportunity size of 2 large or strategic deals, 5 intros to new departments at large or strategic accounts.
        * Field Marketing: Increase operational maturity in field marketing tracking and execution. All event lead uploads within 48 hours, all contacts accurate statuses and notes, accurate campaign data for all field marketing campaigns tracked in salesforce.com.
        * Online Growth: Expand online growth tactics. Increase overall traffic to existing pages by 10% compared to last quarter, increase sign-ups from SEO/PPC/Paid Social by 20% compared to last quarter .
        * Online Growth: Build ad campaigns aligned to cloud and security themes. Double traffic to /cloud-native, 500 MQLs from online advertising.
        * Online Growth: Improve about.gitlab.com conversion rate optimization. Increase live chat leads 20%, increase contact requests by 20%, increase trial sign-ups by 20%.
        * Marketing Program Management: Launch new email nurture series educating trial requesters on .com Gold plan. Keep unsubscribe rate below 1%, 30% of nurture audience at "engaged" progression status or higher.
        * Marketing Program Management: Increase awareness and ongoing education on Gitlab <> Google Partnership. Increase GKE trials referred by Gitlab/week by 3X.
        * Marketing Program Management: Improve reporting for marketing programs. Automate and schedule reporting for email & webcast performance, ensure all email and webcast programs are tracked completely in salesforce.com campaigns.
        * Marketing Operations: Streamline lead management processes. Refreshed lead & contact layouts with all unnecessary fields removed from view, trim down to a single lead queue view for SCA team.
        * Marketing Operations: Deploy multi-touch attribution model in Bizible.
        * Marketing Operations: Ensure all online marketing is tracked as campaigns in salesforce.com. Complete tracking of all required campaign fields for all online marketing.
        * SMB Customer Advocacy: Achieve 200% of SMB IACV plan.
        * SMB Customer Advocacy: Improve SMB account management. Increase SMB gross retention to 90%, increase net retention to 150%, reduce number of manual license key replacements by 30%.

### CEO: Popular next generation product. Complete DevOps GA, GitLab.com ready for mission critical applications, graph of DevOps score vs. cycle time.

* VPE: Make GitLab.com ready for mission-critical customer workloads (99.95% availability)
  * Frontend: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
  * Frontend: Implement 10 [performance improvements](https://gitlab.com/groups/gitlab-org/frontend/-/epics/2): X/10 (X%)
    * Frontend Discussion: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
	* Frontend Discussion: Implement 5 [performance improvements](https://gitlab.com/groups/gitlab-org/frontend/-/epics/3) on the MR/issue page X/10 (X%)
    * Frontend MD&P: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
	* Frontend MD&P: Implement and integrate 10 [reusable Vue components](https://gitlab.com/groups/gitlab-org/frontend/-/epics/1) into GitLab X/10 (X%)
  * Dev Backend
    * Discussion: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
	* Discussion: Implement 10 [performance improvements](https://gitlab.com/groups/gitlab-org/-/epics/247): X/10 (X%)
    * Distribution: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
	* Distribution: GitLab Helm charts generally available
      * Release: Automatically deploy to staging.gitlab.com when creating release
    * Geo: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
	* Geo: Finish failover to Google Cloud July 28
	* Geo: Implement 5 performance improvements (P1/P2/P3 categories): X/5 (X%)
	* Gitaly: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
    * Gitaly: Ship v1.0 and turn off NFS
    * Platform: Implement 10 [performance improvements](https://gitlab.com/groups/gitlab-org/-/epics/246): X/10 (X%)
    * Gitter: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
	* Gitter: Open source Android and iOS applications
    * Gitter: Deprecate Gitter Topics
	* Platform: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
  * Ops Backend
    * CI/CD: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
	* CI/CD: Implement one key architectural performance improvement from https://gitlab.com/gitlab-org/gitlab-ce/issues/46499,
    * Configuration: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
	* Configuration: Fully document and automate Auto DevOps local setup ([#359](https://gitlab.com/gitlab-org/gitlab-development-kit/issues/359))
    * Monitoring: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
	* Monitoring: Implement admin performance dashboad. (Design issue TBD)
    * Security Products: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
	* Security Products: Aggregate and store vulnerabilities occurrences into database
      ([&241](https://gitlab.com/groups/gitlab-org/-/epics/241))
  * Infrastructure: Move to GCP July 28th
    * Database: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
    * Production: Preserve 100% of [error budget](/handbook/engineering/#error-budgets): 100% ()
  * Quality
    * Implement Review Apps for CE/EE with gitlab-qa test automation validation as part of every Merge Request CI pipeline.
    * Triage and add proper priority and severity labels to 80% CE/EE bugs with no priority and severity labels.
  * UX
    * [Establish the User Experience direction for the security dashboard](https://gitlab.com/groups/gitlab-org/-/epics/252): Aid in completing a Competitive analysis, identify 10 must have items for the security dashboard, complete a design artifact/discovery issue.
    * Identify and document the styles for the first 10 components being developed by the FE team.
    * UX Research
      * Incorporate personas into our design process for 100% of our product features.
      * Identify 5 pain points for users who have left GitLab.com. Work with Product Managers to identify solutions.
  * Security
    * Identify and push top 10 sources/attributes to S3 instance and ensure at least 90 day retention and tooling for security investigations.
    * Triage 50 backlogged security labeled issues and add appropriate severity and priority labels.
* CMO: Increase brand awareness and preference for GitLab.
    * Corporate marketing: Help our customers evangelize GitLab. 3 customer or user talk submissions accepted for DevOps related events.
    * Corporate marketing: Drive brand consistency across all events. Company-level messaging and positioning incorporated into pre-event training, company-level messaging and positioning reflected in all event collateral and signage.
    * Corporate marketing: Increase share of voice. 20% lift in web and twitter traffic during event activity.

### CEO: Great team. ELO score per interviewer, Employer branding person, Real-time dashboard for all Key Results.

* VPE: 10 iterations to engineering function handbook: X/10 (X%)
  * Infrastructure: 10 iterations to infrastructure department handbook: X/10 (X%)
    * Production: 10 iterations to production team handbook: X/10 (X%)
  * Ops Backend: 10 iterations to ops backend department handbook: X/10 (X%)
  * Ops Backend: Define and launch a dashboard with 2 engineering metrics for backend teams
  * Quality: 10 iterations to quality department handbook: X/10 (X%)
  * Security: 10 iterations to security department handbook: X/10 (X%)
  * Support: 20 iterations to support department handbook: X/20 (X%)
      * Self-hosted: 10 iterations to self-hosted team handbook focused on process efficiency: X/10 (X%)
      * Services: 10 iterations to services team handbook focused on process efficiency: X/10 (X%)
  * Support: Implement dashboard for key support metrics (SLA, CSAT)
* VPE: Source 50 candidates for various roles: X sourced (X%)
  * Frontend: Source 50 candidates by July 15 and hire 1 manager and 1 engineer: X sourced (X%), hired X (X%)
    * Frontend Discussion: Source 25 candidates by July 15 and hire 1 engineer: X sourced (X%), hired X (X%)
    * Frontend MD&P: Source 25 candidates by July 15 and hire 1 engineer: X sourced (X%), hired X (X%)
  * Dev Backend: Create and apply informative template for team handbook pages
    across department
  * Dev Backend: Create well-defined hiring process for ICs and managers
    documented in handbook, ATS, and GitLab projects
  * Dev Backend: Source 20 candidates by July 15 and hire 1 Gitaly manager: X sourced (0%), hired X (0%)
    * Discussion: Source 50 candidates by July 15 and hire 2 developers: X sourced (X%), hired X (X%)
    * Distribution: Source 15 candidates by July 15 and hire 1 packaging developer: X sourced (X%), hired X (X%)
    * Platform: Source 75 candidates by July 15 and hire 3: X sourced (X%), hired X (X%)
  * Infrastructure: Source 20 candidates by July 15 and hire 1 SRE manager: X sourced (X%), hired X (X%)
    * Database: Source 50 candidates by July 15 and hire 2 DBEs: X sourced (X%), hired X (X%)
    * Production: Source 75 candidates by July 15 and hire 3 SREs: X sourced (X%), hired X (X%)
  * Ops Backend: Source 50 candidates by July 15 and hire monitoring and release managers: X sourced (X%), hired X (X%)
    * CI/CD: Source 60 candidates by July 15 and hire 2 developers: X sourced (X%), hired X (X%)
    * Configuration: Source 60 candidates by July 15 and hire 2 developers: 0 sourced (0%), hired 0 (0%)
    * Monitoring: Source 100 candidates by July 15 and hire 2 developers: X sourced (X%), hired X (X%)
    * Security Products: Source 25 candidates by July 15 and hire 1 developer: 0 sourced (0%), hired 0 (0%)
  * Quality: Source 100 candidates by July 15 and hire 2 test automation engineers: X sourced (X%), hired X (X%)
  * Security: Source 100 candidates by July 15 and hire 3 security team members: X sourced (X%), hired X (X%)
  * Support: Source 50 candidates by July 15 and hire an APAC manager: X sourced (X%), hired X (X%)
    * Self-hosted: Source 350 candidates by July 30 and hire 7 support engineers: X sourced (X%), hired X (X%)
    * Services: Source 200 candidates by July 30 and hire 4 agents: X sourced (X%), hired X (X%)
  * UX: Source 25 candidates by July 15 and hire 1 ux designer: X sourced (X%), hired X (X%)
* CFO: Improve payroll and payments to team members
  * Controller: Analysis and proposal on trinet conversion including cost/benefit analysis of making the move (support from peopleops)
  * Controller: Transition expense reporting approval process to payroll and payments lead.
  * Controller: Full documentation of payroll process (sox compliant)
* CFO: Improve financial performance
  * Fin Ops: Marketing pipeline driven revenue model (need assistance from marketing team)
  * Fin Ops: Recruiting / Hiring Model: redesign gitlab model so that at least 80% of our hiring can be driven by revenue.
  * Fin Ops: Customer support SLA metric on dashboard
* CFO: Build a scalable team
  * Legal: Contract management system for non-sales related contracts.
  * Legal: Implement a vendor management process

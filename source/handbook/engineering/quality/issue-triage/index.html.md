---
layout: markdown_page
title: "Issue Triage"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Common Links

- [**Public Issue Tracker (for GitLab CE)**](https://gitlab.com/gitlab-org/gitlab-ce);
  please use confidential issues for topics that should only be visible to team members at GitLab.
- Chat channels; we use our chat internally as a realtime communication tool:
  - [#triage](https://gitlab.slack.com/messages/triage): general triage team channel.
  - [#gitlab-issue-feed](https://gitlab.slack.com/messages/gitlab-issue-feed) - Feed of all GitLab-CE issues
  - [#support-tracker-feed](https://gitlab.slack.com/messages/support-tracker-feed) - Feed of the GitLab.com Support Tracker

## Issue Triage

Issue Triage Specialists are part of the Quality Team. The role holds multiple responsibilities that typically include Community Support and Development activities.

Most of your time as an Issue Triage Specialist will be spent responding to issues posted to our multiple issue trackers on GitLab.com.

This page indexes resources that Issue Triage Specialists may find useful.

## Resources

* [Issue Triage Policies](/handbook/engineering/issue-triage/)
* [Onboarding](/handbook/quality/issue-triage/onboarding/)
